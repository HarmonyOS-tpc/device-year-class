package com.facebook.device.yearclass;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

//@RunWith(PowerMockRunner.class)
public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.facebook.device.yearclass", actualBundleName);
    }

    @Test
    public void testFileStringValid() {
        assertEquals(DeviceInfo.getCoresFromFileString("0-3"), 4);
        assertEquals(DeviceInfo.getCoresFromFileString("0-11"), 12);
    }

    @Test
    public void testFileStringInvalid() {
        assertEquals(DeviceInfo.getCoresFromFileString("INVALIDSTRING"), -1);
        assertEquals(DeviceInfo.getCoresFromFileString("0-2a"), -1);
        assertEquals(DeviceInfo.getCoresFromFileString("035"), -1);
    }

    /*@PrepareForTest(DeviceInfo.class)
    @Test
    public void testGetYearCategoryS7() {
        // CPU, frequency, RAM, and YearClass values from Samsung Galaxy S7 (global edition).
        int yearClass = getYearClass(8, 2600000, 3663L * 1024 * 1024);
        assertEquals(YearClass.CLASS_2015, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testGetYearCategoryG4() {
        // CPU, frequency, RAM, and YearClass values from LG G4.
        int yearClass = getYearClass(6, 1824000, 2778L * 1024 * 1024);
        assertEquals(YearClass.CLASS_2014, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testGetYearCategoryS5() {
        // CPU, frequency, RAM, and YearClass values from Samsung Galaxy S5.
        int yearClass = getYearClass(4, 2457600, 1946939392L);
        assertEquals(YearClass.CLASS_2013, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testGetYearCategoryGalaxyJ1() {
        // CPU, frequency, RAM, and YearClass values from Samsung Galaxy J1.
        int yearClass = getYearClass(2, 1248000, 716L * 1024 * 1024);
        assertEquals(YearClass.CLASS_2010, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testGetYearCategoryP8lite() {
        // CPU, frequency, RAM, and YearClass values from Huawei P8lite.
        int yearClass = getYearClass(8, 1200000, 1858L * 1024 * 1024);
        assertEquals(YearClass.CLASS_2013, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testEmptyCase() {
        int yearClass = getYearClass(DeviceInfo.DEVICEINFO_UNKNOWN,
                DeviceInfo.DEVICEINFO_UNKNOWN, DeviceInfo.DEVICEINFO_UNKNOWN);
        assertEquals(YearClass.CLASS_UNKNOWN, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testCoreNums() {
        //Test with only number of cores information available.
        int yearClass = getYearClass(4,
                DeviceInfo.DEVICEINFO_UNKNOWN, DeviceInfo.DEVICEINFO_UNKNOWN);
        assertEquals(YearClass.CLASS_2012, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testClockSpeed() {
        //Test with only clock speed information available.
        int yearClass = getYearClass(DeviceInfo.DEVICEINFO_UNKNOWN,
                2457600, DeviceInfo.DEVICEINFO_UNKNOWN);
        assertEquals(YearClass.CLASS_2014, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testTotalRAM() {
        //Test with only total RAM information available.
        int yearClass = getYearClass(DeviceInfo.DEVICEINFO_UNKNOWN,
                DeviceInfo.DEVICEINFO_UNKNOWN, 1946939392L);
        assertEquals(YearClass.CLASS_2013, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testGetYearCategoryS71() {
        // CPU, frequency, RAM, and YearClass values from Samsung Galaxy Note8 (USA edition).
        int yearClass = getYearClass(8, 2350000, 6144L * 1024 * 1024);
        assertEquals(YearClass.CLASS_2016, yearClass);
    }

    @PrepareForTest(DeviceInfo.class)
    @Test
    public void testTotalRAM1() {
        //Test with only total RAM information available.
        int yearClass = getYearClass(DeviceInfo.DEVICEINFO_UNKNOWN,
                DeviceInfo.DEVICEINFO_UNKNOWN, 6144L * 1024 * 1024);
        assertEquals(YearClass.CLASS_2016, yearClass);
    }

    private int getYearClass(int numCores, int maxFreqKHz, long memoryBytes) {
        mockStatic(DeviceInfo.class);
        when(DeviceInfo.getNumberOfCPUCores()).thenReturn(numCores);
        when(DeviceInfo.getCPUMaxFreqKHz()).thenReturn(maxFreqKHz);
        when(DeviceInfo.getTotalMemory((Context) any())).thenReturn(memoryBytes);
        int yearClass = YearClass.get(null);
        PowerMockito.verifyStatic();

        return yearClass;
    }*/
}